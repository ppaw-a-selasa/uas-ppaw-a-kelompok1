### `Anggota Kelompok`

1. Andri Gunawan (1157050018)
2. Badar Jalaluddin Amien (1157050024)
3. Bastomi Maulana Gunawan (11670500xx)
4. Waldi Hidayat (1167050161)
5. Adi Fitrianto (1177050005)
6. Akbar Hidayatullah Harahap (1177050012)
7. Azka Muhammad Ridwan (1177050023)
8. Dadan (1177050026)
9. Dicky Wahyudi (1177050028)

### `Pembagian Tugas`
1. Navbar & Footer = Bastomi M. G. & Waldi H.
2. Homepage = Azka M.R & Adi F.
3. Properties = Dadan
4. Property Detail = Badar J. A. & Andri G.
5. Innerpage = Akbar H. H. & Dicky W.
6. Menggabungkan kodingan (Merging) = Akbar H.H.