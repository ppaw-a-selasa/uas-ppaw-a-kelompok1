import React from 'react';
import './inner.css';
import HeaderImage from '../img/header-bg.jpg';

export default function Header() {
    const style = {fontSize : "32px"};
    const ImageSize = {width: "100%", heigth: "25vh"};
    return (
            <header class="masthead" id="header">
            <div class="container">
                <div class="intro-text">
                    <div class="intro-heading" style={style}>Inner Page with Sidebar</div>
                </div>
            </div>
        </header>
    );
}