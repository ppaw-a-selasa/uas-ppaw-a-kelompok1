import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import Header from './header';
import Container from './container';

function Innerpage() {
  return (
    <div className="Innerpage">
      <Header />
      <Container />
    </div>
  );
}

export default Innerpage;