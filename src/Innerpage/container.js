import React from 'react';
import bg from '../img/02_Innerpage_BGRS.png';
import './inner.css';
import ab from '../img/03_Innerpage_BGRS.png';
import cd from '../img/04_Innerpage_BGRS.png';
import de from '../img/Inner Page.png';

export default function Container() {
    const style = {fontSize : "32px"};
    return(
        <div class="container">
            <div class="row">
                <div class="col-lg-8 explanation">
                    <img src={bg} />
                    <h4><b>Lorem Ipsum dolor sit met , consectetur adipiscing elit</b></h4>
                    <div class="col-sm-6">
                        <img src={ab} alt="" />
                        </div>
                        <div class="col-sm-6"></div>
                    <p>Suspendisse quis nisl fringikka erat commodo finibus id sit amet tellus. 
                        Integer purus mi, aliquam vel gravida at, gravida in tortor. 
                        in lacinia turpis at mollis scelerisque. 
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Nam viverra eros suscipit, consequat exvitae, malesuada erat. 
                        Aliquam nec dapibus leo, a dictum tellus. 
                        Quisque eget dignissim diam, et congue velit. 
                        Lorem ipsum dolor sit amet</p>
                    <img src={cd} alt="" />
                    <div class="row">
                        <div class="col-sm-6">
                            <img src={de} alt="" />
                        </div>
                        <div class="col-sm-6">
                            <h4><b>Lorem Ipsum dolor sit amet , consectetur adipis</b></h4>
                            <p>Lorem ipsum dolor sit amet, consectetur piscing elit. 
                                Nam viverra eros suspicit, consequat ex vitae, malesuada erat. 
                                Aliquam nec dapibus leo, a dictum tellus. 
                                Quisque eget dignissim diam, et congue velit.
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Duis sed leo vel ex congue elementum at insapien.
                                Etiam mattis sit amet elit id laoreet. 
                                Suspendisse mollis, diam in mollis portttitorerat.</p>
                        </div>
                    </div>
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Getting Your Nails Done in a Manly Manner
                                    </button>
                                </h5>
                            </div>
                    
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur piscing elit. 
                                        Nam viverra eros suspicit, consequat ex vitae, malesuada erat. 
                                        Aliquam nec dapibus leo, a dictum tellus. 
                                        Quisque eget dignissim diam, et congue velit.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                        Duis sed leo vel ex congue elementum at insapien. 
                                        Etiam mattis sit amet elit id laoreet. 
                                        Suspendisse mollis, diam in mollis portttitorerat.</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Lorem ipsum dolor sit amet, consectetur adipcising elit
                                    </button>
                                </h5>
                            </div>
                            
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body"></div>
                            </div>
                        </div>
                    
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Cras ultricies turpis ante, in tempor erat tempus on.
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-sm-6">
                            <ul type="square">
                                <li>Nam viverra eros suscipit, consequat ex vitae</li>
                                <li>Vestibulum fermentum varius velit sed curcus</li>
                                <li>Suspendisse viverra lacus eget ultricies porta</li>
                                <li>Suspendisse risus lacus, commodo vitae</li>
                                <li>Lorem ipsum dolor sit amet</li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul type="circle">
                                <li>Nam viverra eros suscipit, consequat ex vitae</li>
                                <li>Vestibulum fermentum varius velit sed </li>
                                <li>Suspendisse viverra lacus eget ultricies porta</li>
                                <li>Suspendisse risus lacus, commodo vitae</li>
                                <li>Lorem ipsum dolor sit amet</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 to-fill">
                    <div class="contact">
                        <h5><b>Contact BGRS</b></h5>
                        <div class="input-group mb-3">
                            <div>
                                <input type="text" class="form-control" placeholder="Your Name" />
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div>
                                <input type="text" class="form-control" placeholder="Email Address" />
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div>
                                <input type="text" class="form-control" placeholder="Phone Number" />
                            </div> 
                        </div>
                        <div class="input-group mb-3">
                            <textarea class="form-control" rows="5" id="comment" placeholder="Message"></textarea>
                        </div>

                        <input type="submit" class="btn btn-info" value="Send" /> 
                    </div>
                    <div class="search">
                        <h5><b>Search</b></h5>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Search Article" />
                        </div>
                    </div>
                    <div class="subscribe">
                        <h5><b>Subscribe</b></h5>
                        <div class="input-group mb-3 sub-form">
                            <input type="text" class="form-control" placeholder="Put Your Email Address" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    );
}