import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';
import Homepage from './components/';
import PropertieDetail from './PropertyDetail/';
import Innerpage from './Innerpage/';
import Layout from './layout/';

function App() {
  return (
    <Router>
      <div>
        <Route exact path = "/" component = {Homepage} />
        <Route path = "/PropertyDetail" component = {PropertieDetail} />
        <Route path = "/Innerpage" component = {Innerpage} />
        <Route path = "/layout" component = {Layout} />
      </div>
    </Router>
  );
}

export default App;
