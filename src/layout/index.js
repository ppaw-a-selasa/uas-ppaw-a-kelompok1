import React from 'react'

import Maps from "./Maps"
import Cardview from "./Cardview"
import Number from "./Number"

function Layout() {
  return (
    <div className="App">
      <Maps />
      <Cardview />
      <Number />
    </div>
  );
}

export default Layout;