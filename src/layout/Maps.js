import React from 'react'

const Maps = () => {
    return(
        <section className="map container text-align-center">
            <div className="align-middle mapsStyle">
                    <div className="mapsView"><iframe width="5000px" height="440" src="https://maps.google.com/maps?width=1350&amp;height=440&amp;hl=en&amp;q=UIN%20Sunan%20Gunung%20Djati+(Title)&amp;ie=UTF8&amp;t=p&amp;z=17&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    <div className="mapsPost"><small className="thisPowered">Powered by <a href="https://embedgooglemaps.com/fr/">Embedgooglemaps FR</a> & <a href="http://lasvegasstatistics.embedgooglemaps.com">las vegas tourism</a></small></div></div><br />
            </div>
    </section>
        )
    }

export default Maps;