import React from 'react'
import { Route, NavLink, HashRouter, Redirect, Switch } from "react-router-dom";
import FeaturedImage1 from '../../img/Featured1.png';
import FeaturedImage2 from '../../img/Featured2.png';
import FeaturedImage3 from '../../img/Featured3.png';
import FeaturedImage4 from '../../img/Featured4.png';
import './Featured.css';

function Featured() {
    return (
        <HashRouter>
        <section className="bg-light page-section">
            <div class="">
                <div class="row">
                    <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Featured Properties</h2>
                    <br></br>
                    </div>
                </div>

                <div className="row">
                    <div class = "col-md-3 col-sm-6 gabung" >
                        <div class="featured-article">
                            <a href="/Innerpage"> <img src={FeaturedImage1} alt="" class="thumb"/> </a>
                            <div class="block-title">
                                <h6 class="text-uppercase">&nbsp;&nbsp;&nbsp;351 Lincoln Road</h6>
                                <p class="by-author"><small>&nbsp;&nbsp;&nbsp;Miami, Florida</small></p>
                            </div>
                        </div>
                    </div>

                    <div class = "col-md-3 col-sm-6 gabung" >
                        <div class="featured-article">
                            <a href="/Innerpage"> <img src={FeaturedImage2} alt="" class="thumb"/> </a>
                            <div class="block-title">
                                <h6 class="text-uppercase">555 Washington Avenue</h6>
                                <p class="by-author"><small>Miami, Florida</small></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 gabung">
                        <div class="featured-article">
                            <a href="/Innerpage"> <img src={FeaturedImage3} alt="" class="thumb"/> </a>
                            <div class="block-title">
                                <h6 class="text-uppercase">1510 Alton Road</h6>
                                <p class="by-author"><small>Miami, Florida</small></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 gabung">
                        <div class="featured-article">
                            <a href="/Innerpage"> <img src={FeaturedImage4} alt="" class="thumb"/> </a>
                            <div class="block-title">
                                <h6 class="text-uppercase">Ave Aviation & Commerce Center</h6>
                                <p class="by-author"><small>Miami, Florida</small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </HashRouter>
    );
}

export default Featured;
