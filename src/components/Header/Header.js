import React from 'react'
import './Header.css';

export default function Header() {
    return (
            <header class="masthead">
                <div class="container">
                    <div class="intro-text">
                        <div class="intro-lead-in"></div>
                        <div class="intro-heading">Next Generation Real Estate</div>
                        <a class="btn btn-success btn-xl batas" href="/Innerpage/">For Sale</a>

                        <a class="btn btn-dark btn-xl batas" href="#">For Lease</a>

                    </div>
                </div>
            </header>
    )
}
