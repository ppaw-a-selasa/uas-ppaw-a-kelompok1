import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import HomeImage from '../../img/homepage-1-mentah.png';


function Home() {
    return (
        <div>
            <img class="img-fluid" src = {HomeImage} alt = "homepage"/>
        </div>
    );
}

export default Home;
