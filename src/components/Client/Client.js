import React from 'react'
import Facebook from '../../img/facebook.png';
import Flipkart from '../../img/flipkart.png';
import Twitter from '../../img/twitter.png';
import Ig from '../../img/ig.png';
import Ebay from '../../img/ebay.png';
import Amazon from '../../img/amazon.png';
import './Client.css';
import 'bootstrap/dist/css/bootstrap.min.css';


export default function Client() {
    return (
    <div className = "client">
            <h2 class="clients">Our Clients</h2>
            <br></br>
            <br></br>
        <div class = "container">
        <div class = "row">
            < div class = "col-md-2" >
                <img src={Facebook}/>
            </div>

            < div class = "col-md-2" >
                <img src={Flipkart}/>
            </div>

            < div class = "col-md-2" >
                <img src={Twitter}/>
            </div>
            
            < div class = "col-md-2" >
                <img src={Ig}/>
            </div>
            
            < div class = "col-md-2" >
                <img src={Ebay}/>
            </div>
            
            < div class = "col-md-2" >
                <img src={Amazon}/>
            </div>
        </div>
        </div>
    </div>
    )
}
