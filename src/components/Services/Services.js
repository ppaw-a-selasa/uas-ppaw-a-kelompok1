import React from 'react'
import Service1 from '../../img/service 01.png';
import Service2 from '../../img/service 02.png';
import Service3 from '../../img/service 03.png';
import Service4 from '../../img/service 04.png';
import './Service.css';

function Services() {
    return (
    <div class="servicess">
    <div class="serv" id="services">
        <div>
            <p className="text-center font">/Our Services</p>
        </div>
        
        <div class = "container">
        <div className = "row">
            <div className="col-md-3 col-sm-3 col-xs-6">
                <img src={Service1} alt=""/>
            </div>

            <div className="col-md-3 col-sm-3 col-xs-6">
                <img src={Service2} alt=""/>
            </div>

            <div className="col-md-3 col-sm-3 col-xs-6">
                <img src={Service3} alt=""/>
            </div>

            <div className="col-md-3 col-sm-3 col-xs-6">
                <img src={Service4} alt=""/>
            </div>
        </div>
        </div>
    </div></div>
    );
}

export default Services;
