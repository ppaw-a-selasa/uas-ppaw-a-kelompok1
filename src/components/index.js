import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import Navbar from './Navbar/Navbar';
import Header from './Header/Header';
import Featured from './Featured/Featured';
import Services from './Services/Services';
import Client from './Client/Client';
import Properties from './Properties/Properties';

function Homepage() {
  return (
    <div className="Homepage">
      <Header />
      <Featured />
      <Properties />
      <Services />
      <Client />
    </div>
  );
}

export default Homepage;