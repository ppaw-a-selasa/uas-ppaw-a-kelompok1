import React, { Component } from "react";
import { Route, NavLink, HashRouter } from "react-router-dom";
import logo from '../img/logo.jpeg';
import slide1 from '../img/slider/2.jpg';
import slide2 from '../img/header-bg.jpg';
import slide3 from '../img/slider/1.jpg';
import slide4 from '../img/slider/header-bg.jpg';
import slide5 from '../img/slider/2.jpg';
import slide6 from '../img/slider/1.jpg';



function PropertyDetail() {
    return (
      <HashRouter>
        <nav className="navbar navbar-expand-lg nabvar-dark fixed-top" id="mainNav">
        <div className="container">
            <a href="#" className="navbar-brand js-scroll-trigger"><img src={logo} alt=""/></a>
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i className="fas fa-bars"></i>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav text-uppercase ml-auto">
                    <li className="nav-item">
                        <a href="index.html" className="nav-link js-scroll-trigger">Home</a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link js-scroll-trigger">About Us</a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link js-scroll-trigger">Services</a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link js-scroll-trigger">Our Properties</a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link js-scroll-trigger">Gallery</a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link js-scroll-trigger">Contact Us</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <header className="masthead">
        <div className="container">
            <div className="intro-text">
                <div className="intro-lead-in">Cutler Square Shopping Center</div>
                <div className="intro-heading text-uppercase">18800 S Dixie Hwy, Cutler Bay, Fl 33157</div>
            </div>
        </div>
    </header>
    <section className="page-section" id="slider">
        <div className="slideshow-container">
            <div className="slide">
                <div className="mySlides">
                    <img src={slide1} style={{height: "500px",width: "100%"}}/>
                </div>
                <div className="mySlides">
                    <img src={slide2} style={{height: "500px",width: "100%"}}/>
                </div>
                <div className="mySlides">
                    <img src={slide3} style={{height: "500px",width: "100%"}}/>
                </div>
                <div className="mySlides">
                    <img src={slide4} style={{height: "500px",width: "100%"}}/>
                </div>
                <div className="mySlides">
                    <img src={slide5} style={{height: "500px",width: "100%"}}/>
                </div>
                <div className="mySlides">
                    <img src={slide6} style={{height: "500px",width: "100%"}}/>
                </div>
            </div>
            <a onclick="plusSlides(-1)" className="prev"><i class="fas fa-angle-double-left"></i> </a>
            <a onclick="plusSlides(1)" className="next"><i class="fas fa-angle-double-right"></i> </a>
            <div className="row">
                <div className="column">
                    <img className="demo cursor" src={slide5} style={{height: "150px", width: "100%"}} onclick="currentSlide(1)" alt="The Woods"/>
                </div>
                <div className="column">
                    <img className="demo cursor" src={slide4} style={{height: "150px", width: "100%"}} onclick="currentSlide(2)" alt="Cinque Terre"/>
                </div>
                <div className="column">
                    <img className="demo cursor" src={slide6} style={{height: "150px", width: "100%"}} onclick="currentSlide(3)" alt="Mountains and fjords"/>
                </div>
                <div className="column">
                    <img className="demo cursor" src={slide4} style={{height: "150px", width: "100%"}} onclick="currentSlide(4)" alt="Northern Lights"/>
                </div>
                <div className="column">
                    <img className="demo cursor" src={slide5} style={{height: "150px", width: "100%"}} onclick="currentSlide(5)" alt="Nature and sunrise"/>
                </div>    
                <div className="column">
                    <img className="demo cursor" src={slide6} style={{height: "150px", width: "100%"}} onclick="currentSlide(6)" alt="Snowy Mountains"/>
                </div>
            </div>
            <div style={{textAlign:"center"}} id="dot">
                <span className="dot"></span> 
                <span className="dot" ></span> 
                <span className="dot" ></span> 
            </div>
        </div>
    </section>
    <section className="bg-light page-section" id="specs">
        <div className="container">
            <div className="row">
                <div className="col-lg-12 text-center">
                    <h2 className="section-heading text-uppercase">/Specs</h2>
                </div>
            </div>
            <table align="center">
            <tr>
              <th>Name :</th>
              <td>AVE Aviation</td>
              <td></td>
              <th>Property Type :</th>
              <td>T-5 Lightning</td>
            </tr>
            <tr>
                <th>Address :</th>
                <td>NW 57th Avenue & NW 145th Street</td>
                <td></td>
                <th>Property Sub-type :</th>
                <td>2015</td>
            </tr>
            <tr>
                <th>Total Space Avaible :</th>
                <td>+/-5.636 SF</td>
                <td></td>
                <th>Building Size :</th>
                <td>Miami+Dade</td>
            </tr>
            <tr>
                <th>Rental Rate : Negotiable</th>
                <td>+/-37.243 SF</td>
                <td></td>
                <th>Lot Size :</th>
                <td>GP(Gov't Property</td>
            </tr>
            <tr>
                <th>Min.Divisible :</th>
                <td>N/A</td>
                <td></td>
                <th>Construction Status :</th>
                <td>N/A</td>
            </tr>
            <tr>
                <th>Max. Contiguos :</th>
                <td>15 Rear Loading Door</td>
                <td></td>
                <th>Build to Suite :</th>
                <td>Tilt-Wall</td>
            </tr>
        </table>
        <div className="row" style={{padding: "20px;"}}>
            <div className="col-lg-6 text-center">
                <div className="form-group">
                    <p className="fa fa-download"> Property Flayer</p>
                </div>
            </div>
            <div className="col-lg-6 text-center">
                    <div className="form-group">
                <p className="fa fa-download"> Demographics PDF</p>
            </div>
            </div>
        </div>
        </div>
    </section>
    <section className="page-section" id="contact">
        <div className="container">
            <div className="row">
                <div className="col-lg-12 text-center">
                    <h2 className="section-heading text-uppercase">/Contact BGRS</h2>
                </div>
            </div>
            <div className="row">
                    <div className="col-lg-12">
                      <form id="contactForm" name="sentMessage" novalidate="novalidate">
                        <div className="row">
                          <div className="col-md-4">
                            <div className="form-group">
                              <input className="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name."/>
                              <p className="help-block text-danger"></p>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                              <input className="form-control" id="email" type="email" placeholder="Email Address *" required="required" data-validation-required-message="Please enter your email address."/>
                              <p className="help-block text-danger"></p>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                              <input className="form-control" id="phone" type="tel" placeholder="Phone Number *" required="required" data-validation-required-message="Please enter your phone number."/>
                              <p className="help-block text-danger"></p>
                            </div>
                        </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <textarea className="form-control" id="message" placeholder="Space Requirenent *" required="required" data-validation-required-message="Please enter a message."></textarea>
                              <p className="help-block text-danger"></p>
                            </div>
                          </div>
                          <div className="col-md-6">
                                <div className="form-group">
                                  <textarea className="form-control" id="message" placeholder="user Requirenent *" required="required" data-validation-required-message="Please enter a message."></textarea>
                                  <p className="help-block text-danger"></p>
                                </div>
                              </div>
                          <div className="clearfix"></div>
                          <div className="col-lg-12 text-center">
                            <div id="success"></div>
                            <button id="sendMessageButton" className="btn btn-primary btn-xl text-uppercase" type="submit">Send</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
        </div>
    </section>
    <section className="page-section" id="map">
        <div className="container">
            <div className="row">
                <div className="col-lg-2"></div>
                <div className="col-lg-4 text-left">
                <div className="form-group">
                    <h2 className="fa fa-home">  Property Description</h2>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Recusandae ipsam earum eaque mollitia porro aperiam excepturi natus! Exercitationem consectetur recusandae et in, vero culpa. Quo, natus ea. Eius, ratione exercitationem.</p>
                </div>
            </div>
            <div className="col-lg-4 text-left">
                    <div className="form-group">
                <h2 className="fa fa-map">  Location Description</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum pariatur beatae cumque harum minus qui laudantium error maxime, adipisci perspiciatis nam ab reiciendis optio voluptas iste id obcaecati culpa sapiente?</p>
            </div>
            </div>
            </div>
            <div id="googleMap" style={{width: "100%",height: "400px"}}>
            
            </div>
        </div>
    </section>
    <section className="page-section" id="spaces">
        <div className="container">
            <div className="row">
                <div className="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">/2 Spaces Avaible</h2>
                </div>
                <div className="col-lg-12 text-center">
                <input type="checkbox"/>Leased
                <input type="checkbox"/> Sold
            </div>
            </div>
            <table  >
                <h3>Space End Cap</h3>
            <tr>
              <th>Rental Rate</th>
              <td>Negotiable</td>
              <td></td>
              <th>Max. Contiguos</th>
              <td>6.000 SF</td>
            </tr>
            <tr>
                <th>Space Avaible</th>
                <td>5.379 SF</td>
                <td></td>
                <th>Space Type</th>
                <td>Community Center</td>
            </tr>
            <tr>
                <th>Min. Divisible</th>
                <td>4.000 SF</td>
                <td></td>
                <th>Lease Type</th>
                <td>NNN</td>
            </tr>
        </table>
        <table >
                <h3 className="anc">Anchor Space</h3>
            <tr>
              <th>Rental Rate</th>
              <td>Negotiable</td>
              <td></td>
              <th>Max. Contiguos</th>
              <td>6.000 SF</td>
            </tr>
            <tr>
                <th>Space Avaible</th>
                <td>5.379 SF</td>
                <td></td>
                <th>Space Type</th>
                <td>Community Center</td>
            </tr>
            <tr>
                <th>Min. Divisible</th>
                <td>4.000 SF</td>
                <td></td>
                <th>Lease Type</th>
                <td>NNN</td>
            </tr>
        </table>
        </div>
    </section>
    <section className="page-section" id="contact">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 text-center">
                        <h2 className="section-heading text-uppercase">/Contact BGRS</h2>
                    </div>
                </div>
                <div className="row">
                        <div className="col-lg-12">
                          <form id="contactForm" name="sentMessage" novalidate="novalidate">
                            <div className="row">
                              <div className="col-md-4">
                                <div className="form-group">
                                  <input className="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name."/>
                                  <p className="help-block text-danger"></p>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                  <input className="form-control" id="email" type="email" placeholder="Email Address *" required="required" data-validation-required-message="Please enter your email address."/>
                                  <p className="help-block text-danger"></p>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                  <input className="form-control" id="phone" type="tel" placeholder="Phone Number *" required="required" data-validation-required-message="Please enter your phone number."/>
                                  <p className="help-block text-danger"></p>
                                </div>
                            </div>
                              <div className="col-md-6">
                                <div className="form-group">
                                  <textarea className="form-control" id="message" placeholder="Space Requirenent *" required="required" data-validation-required-message="Please enter a message."></textarea>
                                  <p className="help-block text-danger"></p>
                                </div>
                              </div>
                              <div className="col-md-6">
                                    <div className="form-group">
                                      <textarea className="form-control" id="message" placeholder="user Requirenent *" required="required" data-validation-required-message="Please enter a message."></textarea>
                                      <p className="help-block text-danger"></p>
                                    </div>
                                  </div>
                              <div className="clearfix"></div>
                              <div className="col-lg-12 text-center">
                                <div id="success"></div>
                                <button id="sendMessageButton" className="btn btn-primary btn-xl text-uppercase" type="submit">Send</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
            </div>
        </section>



      </HashRouter>
    );
  }


export default PropertyDetail;
